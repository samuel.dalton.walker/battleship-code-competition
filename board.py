class Board:
    def __init__(self, gridSpaces, default=None):
        self.n = gridSpaces
        self.gameBoard = self._setUpBoard()

        if default is not None:
            for space in default:
                self.gameBoard[space[0]][space[1]] = "B"

        # ADD VARIABLES HERE ....

    def _setUpBoard(self):
        board = [[""] * self.n for _ in range(self.n)]
        return board

    def placeShip(self, x, y):
        self.gameBoard[x][y] = "B"

        # DO LOGIC HERE (IF NEEDED) ....

    # vvvvv MAIN FUNCTION vvvvv
    def calculateValidOptions(self):
        # DO LOGIC HERE ....
        return []

    def printGameBoard(self):
        for i in range(self.n):
            print(self.gameBoard[i])
        print("\n\n")