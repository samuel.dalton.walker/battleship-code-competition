import board

class TestBed:
    # "X" "" "" "" ""
    # "" "B" "" "" ""
    # "" "" "X" "" ""
    # "" "" "" "B" ""
    # "" "" "" "" "X"
    def TestCase1(self):
        answers = [(0,0), (2,2), (4,4)]
        gameBoard = board.Board(5)

        gameBoard.placeShip(3, 3)
        gameBoard.placeShip(1, 1)

        valid_options = gameBoard.calculateValidOptions()

        assert set(answers) == set(valid_options)

        gameBoard.printGameBoard()

    # "B" "" "" "" ""
    # "" "B" "" "" ""
    # "" "" "X" "" ""
    # "" "" "" "B" ""
    # "" "" "" "" ""
    def TestCase2(self):
        answers = [(2,2)]
        gameBoard = board.Board(5)

        gameBoard.placeShip(3, 3)
        gameBoard.placeShip(1, 1)
        gameBoard.placeShip(0, 0)

        valid_options = gameBoard.calculateValidOptions()

        assert set(answers) == set(valid_options)

        gameBoard.printGameBoard()

    # "" "" "" "" "" "" "" "" "" "" "" "" ""
    # "" "" "" "" "X" "" "" "" "" "" "" "" ""
    # "" "" "" "" "" "X" "" "" "" "" "" "" ""
    # "" "" "" "" "" "" "B" "" "" "" "" "" ""
    # "" "" "" "" "" "" "" "B" "" "" "" "" ""
    # "" "" "" "" "" "" "" "" "X" "" "" "" ""
    # "" "" "" "" "" "" "" "" "" "X" "" "" ""
    # "" "" "" "" "" "" "" "" "" "" "" "" ""
    # "" "" "" "" "" "" "" "" "" "" "" "" ""
    # "" "" "" "" "" "" "" "" "" "" "" "" ""
    # "" "" "" "" "" "" "" "" "" "" "" "" ""
    # "" "" "" "" "" "" "" "" "" "" "" "" ""
    # "" "" "" "" "" "" "" "" "" "" "" "" ""
    def TestCase3(self):
        answers = [(4, 1), (5, 2), (8, 5), (9, 6)]
        gameBoard = board.Board(13)

        gameBoard.placeShip(6, 3)
        gameBoard.placeShip(7, 4)

        valid_options = gameBoard.calculateValidOptions()

        assert set(answers) == set(valid_options)

        gameBoard.printGameBoard()

    # "" "" "" "" "" "" "" "" "" "" "" "" ""
    # "" "" "" "" "" "" "" "" "" "" "" "" ""
    # "" "" "" "" "" "" "" "" "" "" "" "" ""
    # "" "" "" "" "" "" "" "" "" "" "" "" ""
    # "" "" "" "" "" "" "" "" "" "" "" "" ""
    # "" "" "" "" "" "" "" "" "" "" "" "" ""
    # "" "" "" "" "" "" "" "" "" "" "" "" ""
    # "" "" "" "" "" "" "B" "" "" "" "" "" ""
    # "" "" "" "" "" "" "X" "" "" "" "" "" ""
    # "" "" "" "" "" "" "X" "" "" "" "" "" ""
    # "" "" "" "" "" "" "B" "" "" "" "" "" ""
    # "" "" "" "" "" "" "" "" "" "" "" "" ""
    # "" "" "" "" "" "" "" "" "" "" "" "" ""
    def TestCase4(self):
        answers = [(6, 8), (6, 9)]
        gameBoard = board.Board(13)

        gameBoard.placeShip(6, 7)
        gameBoard.placeShip(7, 10)

        valid_options = gameBoard.calculateValidOptions()

        assert set(answers) == set(valid_options)

        gameBoard.printGameBoard()

    def RunTestCases(self):
        try:
            self.TestCase1()
            self.TestCase2()
            self.TestCase3()
            self.TestCase4()
        except:
            print("FAILED A TESTCASE")